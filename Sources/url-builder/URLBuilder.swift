import Foundation

public extension URL {
    struct Builder {
        public static let http = Builder.scheme("http")
        public static let https = Builder.scheme("https")
        
        private let scheme: String?
        private let host: String?
        private let port: Int?
        private let paths: [String]
        private let queryItems: [URLQueryItem]?

        public func build() -> URL? {
            var urlComponents = URLComponents()
            urlComponents.scheme = scheme
            urlComponents.host = host
            urlComponents.port = port
            urlComponents.queryItems = queryItems
            
            if !paths.isEmpty {
                urlComponents.path = "/" + paths.joined(separator: "/")
            }

            return urlComponents.url
        }
        
        public static func scheme(_ scheme: String) -> Builder {
            Builder(scheme: scheme, host: nil, port: nil, paths: [], queryItems: nil)
        }

        public func scheme(_ scheme: String) -> Builder {
            Builder(scheme: scheme, host: host, port: port, paths: paths, queryItems: queryItems)
        }

        public func host(_ host: String) -> Builder {
            Builder(scheme: scheme, host: host, port: port, paths: paths, queryItems: queryItems)
        }

        public func port(_ port: Int) -> Builder {
            Builder(scheme: scheme, host: host, port: port, paths: paths, queryItems: queryItems)
        }

        public func appendPath(_ newSegment: String) -> Builder {
            Builder(scheme: scheme, host: host, port: port, paths: paths + [newSegment], queryItems: queryItems)
        }

        public func appendQueryParameter(key: String, value: String) -> Builder {
            Builder(scheme: scheme,
                    host: host,
                    port: port,
                    paths: paths,
                    queryItems: (queryItems ?? []) + [URLQueryItem(name: key, value: value)])
        }
    }
}
