import XCTest

import url_builderTests

var tests = [XCTestCaseEntry]()
tests += url_builderTests.allTests()
XCTMain(tests)
