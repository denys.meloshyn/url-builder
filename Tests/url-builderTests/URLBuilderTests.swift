import XCTest
@testable import url_builder

final class URLBuilderTests: XCTestCase {
    func test_build_schemaExist() {
        XCTAssertEqual(URL.Builder.https.build(), URL(string: "https:"))
    }

    static var allTests = [
        ("test_build_schemaExist", test_build_schemaExist),
    ]
}
